#
#  OpenVideoConverter
#
#  Created by Julian Groß on 2022.08.02
#  Copyright 2022 Overte e.V.
#
#  Distributed under the Apache License, Version 2.0.
#  See the accompanying file LICENSE or http://www.apache.org/licenses/LICENSE-2.0.html
#

import argparse
from pathlib import Path
from subprocess import run

from logger import Log
from ffmpeg import FFmpeg


__version__ = "v0.0.4-dev"


parser = argparse.ArgumentParser(
    description="Convert videos to open formats compatible with Overte.",
    epilog="Report issues to https://codeberg.org/JulianGro/OpenVideoConverter/issues",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("input", help="Path of input file", type=Path)
parser.add_argument("output", help="Where to save the output file", type=Path)
parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="count", default=None)
parser.add_argument("--version", help="Display program version", action="version", version="%(prog)s " + __version__)
parser.add_argument("--vcodec", help="Manually choose which video codec to use",
                    choices=["av1", "vp9", "vp8"], default="vp9")
parser.add_argument("--acodec", help="Manually choose which audio codec to use",
                    choices=["libopus", "libvorbis"], default="libopus")
parser.add_argument("--ffmpeg_executable", help="Manually set which FFMPEG execuatable to use",
                    metavar="ffmpeg", default="ffmpeg")
arguments = parser.parse_args()


log = Log(arguments.verbose)
debug, info, warn, error  = log.debug, log.info, log.warn, log.error

ffmpeg = FFmpeg(arguments.ffmpeg_executable, arguments.verbose)


ffmpeg.check_availability()
ffmpeg.check_encoders()


ffmpeg_command = [arguments.ffmpeg_executable,
                  "-i", arguments.input,
                  "-f", "webm",
                  "-vcodec", arguments.vcodec,
                  "-acodec", arguments.acodec,
                  arguments.output
                  ]

ffmpeg_command = ffmpeg.set_verbosity(ffmpeg_command)


debug(ffmpeg_command)
run(ffmpeg_command)
