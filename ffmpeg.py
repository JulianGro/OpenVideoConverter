#
#  ffmpeg.py
#
#  Created by Julian Groß on 2022.08.04
#  Copyright 2022 Overte e.V.
#
#  Distributed under the Apache License, Version 2.0.
#  See the accompanying file LICENSE or http://www.apache.org/licenses/LICENSE-2.0.html
#

from logger import Log
from subprocess import run, check_output
import os


class FFmpeg:
    def __init__(self, ffmpeg_executable, verbosity: int):
        self.ffmpeg_executable = ffmpeg_executable
        self.verbosity = verbosity
        log = Log(self.verbosity)
        self.debug, self.info, self.warn, self.error  = log.debug, log.info, log.warn, log.error

    def set_verbosity(self, ffmpeg_command: list):
        if self.verbosity == 0 or self.verbosity is None:
            ffmpeg_command[1:1] = ["-hide_banner", "-loglevel", "error", "-stats"]
            return ffmpeg_command
        elif self.verbosity == 1:
            ffmpeg_command[1:1] = ["-hide_banner", "-loglevel", "warning", "-stats"]
            return ffmpeg_command
        elif self.verbosity == 2:
            ffmpeg_command[1:1] = ["-hide_banner", "-loglevel", "info"]
            return ffmpeg_command
        else:  # verbosity >= 3
            ffmpeg_command[1:1] = ["-loglevel", "info"]
            return ffmpeg_command

    def check_availability(self):
        try:
            ffmpeg_command = [self.ffmpeg_executable, "-version"]
            run(ffmpeg_command, capture_output=True)
        except Exception:
            self.log.error(f"Could not find FFMPEG executable under \"{self.ffmpeg_executable}\"")

    def get_encoders(self):
        ffmpeg_encoders = check_output([self.ffmpeg_executable, "-encoders", "-hide_banner"])
        ffmpeg_encoders = ffmpeg_encoders.decode("utf-8")
        ffmpeg_encoders = ffmpeg_encoders.splitlines()
        return ffmpeg_encoders

    def check_for_encoder(self, encoder: str, encoders: list):
        found = False
        for e in encoders:
            if encoder in e:
                found = True
                break
        self.info(f"Found {encoder} = {str(found)}")
        return found

    def check_hardware_acceleration(self, encoder: str):
        if encoder in ["vp9_vaapi", "vp9_qsv", "vp8_vaapi"]:
            file_name = f"test_file_{encoder}.webm"
            ffmpeg_command = [self.ffmpeg_executable,
                              "-i", "test_file.jpg",
                              "-vcodec", encoder, "-f", "webm", "-y", file_name]
            run(ffmpeg_command, capture_output=True)
            if os.path.getsize(file_name) > 0:
                found = True
            else:
                found = False
            os.remove(file_name)
            self.info(f"Hardware acceleration {encoder} = {str(found)}")
            return found

    def check_encoders(self):
        interesting_encoders = []
        encoders = self.get_encoders()
        for e in ["libvpx-vp9", "libsvt_vp9", "vp9_vaapi", "vp9_qsv",
                  "libvpx ", "vp8_vaapi",
                  "libaom-av1", "libsvtav1",
                  "libopus",
                  "libvorbis",
                  ]:
            if self.check_for_encoder(e, encoders) is True:
                interesting_encoders.append(e)
        self.debug(interesting_encoders)

        for e in interesting_encoders:
            if self.check_hardware_acceleration(e) is False:
                interesting_encoders.remove(e)
        self.debug(interesting_encoders)
