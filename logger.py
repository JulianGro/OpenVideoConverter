#
#  logger.py
#
#  Created by Julian Groß on 2022.08.04
#  Copyright 2022 Overte e.V.
#
#  Distributed under the Apache License, Version 2.0.
#  See the accompanying file LICENSE or http://www.apache.org/licenses/LICENSE-2.0.html
#

class Log:
    def __init__(self, log_level: int):
        self.log_level = log_level

    def debug(self, message: str):
        if self.log_level >= 3:
            print("debug:", message)

    def info(self, message: str):
        if self.log_level >= 2:
            print("info:", message)

    def warn(self, message: str):
        if self.log_level >= 1:
            print("warning:", message)

    def error(self, message: str):
        print("error:", message)
